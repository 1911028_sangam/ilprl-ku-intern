def find_max_min(numbers):

    max_value = numbers[0]
    min_value = numbers[0]

    # Traverse the array to find the maximum and minimum values
    for num in numbers:
        if num > max_value:
            max_value = num
        if num < min_value:
            min_value = num

    return max_value, min_value

array = [5, 3, 9, 2, 7, 1, 8, 4, 6, 1]

max_val, min_val = find_max_min(array)

print("Maximum value:", max_val)
print("Minimum value:", min_val)
