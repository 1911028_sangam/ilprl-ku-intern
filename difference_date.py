from datetime import datetime

def calculate_date_difference(start_date, end_date):
    date_format = "%d-%m-%Y"

    # Parsing the dates into datetime objects
    start_datetime = datetime.strptime(start_date, date_format)
    end_datetime = datetime.strptime(end_date, date_format)

    difference = end_datetime - start_datetime

    # Converting the difference to months and years if applicable
    if difference.days > 30:
        months = difference.days // 30
        if months > 12:
            years = months // 12
            months %= 12

            return f"{years} years, {months} months"
        else:
            return f"{months} months"
    else:
        return f"{difference.days} days"

start_date = input("Enter the start date (dd-mm-yyyy): ")
end_date = input("Enter the end date (dd-mm-yyyy): ")

result = calculate_date_difference(start_date, end_date)

print("Date difference is:", result)