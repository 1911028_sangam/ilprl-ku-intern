def invert_and_sort(user_input):
    alphabets = ""
    num = ""
    for char in user_input:
        if char.isalpha():
            alphabets += char
        elif char.isdigit():
            num += char

    inverted_string = alphabets[::-1]
    sorted_numbers = ''.join(sorted(num, reverse=True))

    final = inverted_string + sorted_numbers
    print("Inverted string with numbers at the end (in descending order) is: ", final)

user_input = input("Enter a string: ")

invert_and_sort(user_input)