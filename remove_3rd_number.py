numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

while len(numbers) >= 3:
    third_number = numbers[2]
    print("Current 3rd number:", third_number)
    numbers.remove(third_number)
    # numbers.pop(2) # Remove the 3rd number from the list, You can choose either

print("No 3rd numbers left.")